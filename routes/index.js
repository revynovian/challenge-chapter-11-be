const express = require('express');
const router = express.Router();
const userRoutes = require('./user')
const adminRoutes = require('./admin')

router.use('/user', userRoutes );
router.use('/admin', adminRoutes );
router.use('/test', (req, res) => {
  res.json('API is working !')
} );

module.exports = router;
